FROM python:3

WORKDIR /usr/src/app

RUN python3 -m venv env

COPY . .

RUN . env/bin/activate \
    && pip3 install --no-cache-dir -r requirements.txt


RUN apt-get update \
    && apt-get install apt-file -y \
    && apt-file update \
    && apt-get install vim -y

RUN apt-get install supervisor -y
COPY ./src/supervisor /etc/supervisor


COPY ./src/scripts/docker-entrypoint.sh /usr/local/bin
RUN chmod 777 /usr/local/bin/docker-entrypoint.sh \
    && ln -s /usr/local/bin/docker-entrypoint.sh /


CMD ["docker-entrypoint.sh"]
