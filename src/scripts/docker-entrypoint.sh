#!/usr/bin/env bash

cd /usr/src/app
source env/bin/activate
python3 manage.py makemigrations
python3 manage.py makemigrations images_api
python3 manage.py migrate

supervisord
