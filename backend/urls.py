"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.views.generic import TemplateView
from rest_framework.routers import DefaultRouter
from django.conf.urls.static import static
from django.conf import settings
from rest_auth.registration.views import VerifyEmailView
from allauth.account.views import ConfirmEmailView

from backend.images_api.views import index
from backend.images_api.views import TaskViewSet

router = DefaultRouter(trailing_slash=True)
router.register(r'', TaskViewSet)

urlpatterns = [
    url(r'^api/v1/rest-auth/registration/account-confirm-email/(?P<key>[-:\w]+)/$', ConfirmEmailView.as_view(),
        name="account_confirm_email"),
    path('admin/', admin.site.urls),
    url(r'^api/v1/rest-auth/', include('rest_auth.urls')),
    url(r'^api/v1/rest-auth/registration/', include('rest_auth.registration.urls')),
    # path('', AccountViewSet.as_view()),
    # path('', include(router.urls)),
    path('login/', TemplateView.as_view(template_name="images_api/log.html")),
    path('sign/', TemplateView.as_view(template_name="images_api/sign.html")),
    path('', include(router.urls), name='index'),

    url(r'^api/v1/account-confirm-email/', VerifyEmailView.as_view(), name='account_email_verification_sent'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
