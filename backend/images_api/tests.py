from django.test import TestCase
from django.test import Client

class SimpleTest(TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_login(self):
        # Issue a GET request.
        response = self.client.get('/login/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

    def test_sign(self):
        # Issue a GET request.
        response = self.client.get('/sign/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

    def test_index(self):
        # Issue a GET request.
        response = self.client.get('/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 302)


