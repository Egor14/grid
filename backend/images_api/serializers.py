from rest_framework import serializers
import json

from backend.images_api.models import Task
from backend.images_api.sender import send


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['id', 'name', 'link', 'status', 'image']
        extra_kwargs = {
            'status': {'read_only': True},
            'image': {'read_only': True}
        }

    def create(self, validated_data):
        instance = super().create(validated_data)
        data = {
            'image_id': instance.id,
            'image_link': instance.link
        }
        send(json.dumps(data))
        return instance

