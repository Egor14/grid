from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    name = models.CharField(max_length=100)
    link = models.TextField()
    status = models.BooleanField(default=False)
    image = models.ImageField(upload_to='', null=True, default=None)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
