from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework import viewsets

from backend.images_api.serializers import TaskSerializer
from backend.images_api.models import Task


@login_required(login_url='/login')
def index(request):
    return render(request, 'images_api/index.html')


class TaskViewSet(LoginRequiredMixin, viewsets.ModelViewSet):
    login_url = '/login/'
    redirect_field_name = '/'

    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        result = self.queryset.filter(user=self.request.user)
        return result

    def perform_create(self, serializer):
        # when a product is saved, its saved how it is the owner
        serializer.save(user=self.request.user)
