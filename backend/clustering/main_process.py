from PIL import Image
import requests
import os
import cv2


def clustering_handler(data):
    image_link = data['image_link']

    image_response = requests.get(image_link)
    print('preparing', flush=True)
    with open('image.jpg', 'wb') as f:
        f.write(image_response.content)

    print('start', flush=True)

    image = cv2.imread('image.jpg')
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = cv2.resize(image, (int(image.shape[1] * 0.5), int(image.shape[0] * 0.5)))
    image = Image.fromarray(image)
    image.save('new.jpg')

    with open('new.jpg', 'rb') as f:
        image_bytes = f.read()

    print('done', flush=True)

    os.remove('image.jpg')
    os.remove('new.jpg')

    print('removed', flush=True)

    return image_bytes
