import pika
import sys
import os
import json

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(BASE_DIR)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
import django
django.setup()
from django.core.files.base import ContentFile

from backend.clustering.main_process import clustering_handler
from backend.images_api.models import Task


class TaskReceiver:
    def start(self):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
        channel = connection.channel()

        channel.queue_declare(queue='task_queue', durable=True)
        print(' [*] Waiting for messages. To exit press CTRL+C', flush=True)

        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(queue='task_queue', on_message_callback=self.callback)

        channel.start_consuming()

    def callback(self, ch, method, properties, body):
        print(" [x] Received %r" % body, flush=True)
        data = json.loads(body.decode())
        result = clustering_handler(data)
        print('result', result, flush=True)

        instance = Task.objects.get(id=data['image_id'])
        instance.image.save(name=str(instance.id) + '.jpg', content=ContentFile(bytes(result)))

        print(" [x] Done", flush=True)
        ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    receiver = TaskReceiver()
    receiver.start()
