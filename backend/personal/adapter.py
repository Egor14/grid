from django.core.mail import send_mail
from allauth.account.adapter import DefaultAccountAdapter
from django.contrib.sites.shortcuts import get_current_site

from backend.personal.tasks import send_mail_task


class SubSiteRegistrationAdapter(DefaultAccountAdapter):
    # def send_confirmation_mail(self, request, emailconfirmation, signup):
    # super().send_confirmation_mail(request, emailconfirmation, signup)
    def send_confirmation_mail(self, request, emailconfirmation, signup):
        current_site = get_current_site(request)
        activate_url = self.get_email_confirmation_url(
            request,
            emailconfirmation)
        ctx = {
            "user": emailconfirmation.email_address.user,
            "activate_url": activate_url,
            "current_site": current_site,
            "key": emailconfirmation.key,
        }
        if signup:
            email_template = 'account/email/email_confirmation_signup'
        else:
            email_template = 'account/email/email_confirmation'
        # self.send_mail(email_template,
        #                emailconfirmation.email_address.email,
        #                ctx)
        send_mail_task.delay('Hello from image maker!!!', activate_url, 'gruuuuny@gmail.com',
                             [emailconfirmation.email_address.email])
