# Create your tasks here
from __future__ import absolute_import, unicode_literals

from celery import shared_task
from django.core.mail import send_mail
from allauth.account.adapter import DefaultAccountAdapter


@shared_task
def send_mail_task(subject, message, from_mail, to_mails):
    send_mail(
        subject,
        message,
        from_mail,
        to_mails,
        fail_silently=False,
    )
